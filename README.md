## **Requerimientos de instalación**:

- **Docker**
- **Go**

### **Comandos**:

ejecutar el siguiente comando desde la ruta por consola donde se encuentre el archivo **docker-compose.yml**

`docker-compose build`

Después que se descargaron las respectivas imágenes utilizar el comando:

`docker-compose up`

y esperar a que **mongo** y **mongo-express** se inicializen.

Una vez que mongo y express están en funcionamiento con docker, puedes ir a **http://localhost:8081/** para verificar su conexión donde te solicitará un user y pass los cuáles son los siguientes:

- user: **cidenet**
- password: **12345**

Después ya solamente es cuestión de ejecutar el comando:

`go run main.go`

para que inicialize el servidor del backend, allí mostrará un mensaje de confirmación que dirá "**Connected to MongoDB!**" de esta manera podrás saber que se inicializó y conectó a los contenedores de mongo de manera correcta.

### **Endpoints**:

- **http://localhost:9000/employees** [GET]
- **http://localhost:9000/employee** [POST]
- **http://localhost:9000/employee/** {_aquí va el userid de mongo_} [GET]
- **http://localhost:9000/employee/** {_aquí va el userid de mongo_} [PUT]
- **http://localhost:9000/employee/** {_aquí va el userid de mongo_} [DELETE]

## **IMPORTANTE**:

- **No estoy muy seguro si los paquetes de go necesiten instalarse de nuevo, pero en caso de que se requiera en el archivo "_requirements.txt_" pueden encontrar los comandos.**
- **Antes de ejecutar el código del frontend, primero tener ejecutando el código del backend, de lo contrario la aplicación no funcionará de la manera adecuada.**
