package main

import (
	"employees/controllers"
	"employees/database"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
)

func main() {
	database.DbConnection()
	router := controllers.Router()
	// Where ORIGIN_ALLOWED is like `scheme://dns[:port]`, or `*` (insecure)
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	// start server listen
	// with error handling
	log.Fatal(http.ListenAndServe(":9000", handlers.CORS(originsOk, headersOk, methodsOk)(router)))
}
