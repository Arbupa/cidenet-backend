package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Employee struct {
	ID             primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	FirstLastName  string             `json:"first_last_name" bson:"first_last_name"`
	SecondLastName string             `json:"second_last_name" bson:"second_last_name"`
	FirstName      string             `json:"first_name" bson:"first_name"`
	OtherNames     string             `json:"other_names" bson:"other_names"`
	CountryWorking string             `json:"country_working" bson:"country_working"`
	Domain         string             `json:"domain" bson:"domain"`
	IDType         string             `json:"id_type" bson:"id_type"`
	IDNumber       string             `json:"id_number" bson:"id_number"`
	Email          string             `json:"email" bson:"email"`
	AdmissionDate  string             `json:"admission_date" bson:"admission_date"`
	Area           string             `json:"area" bson:"area"`
	State          string             `json:"state" bson:"state"`
	RegisterDate   string             `json:"register_date" bson:"register_date"`
	UpdateDate     string             `json:"update_date" bson:"update_date"`
}
