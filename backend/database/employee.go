package database

import (
	"context"
	"employees/models"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	EmployeesCollection *mongo.Collection
	Ctx                 = context.TODO()
)

// function to creates and return the connection with mongo
func DbConnection() *mongo.Client {

	credentials := options.Credential{
		Username: "root",
		Password: "example",
	}

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017").SetAuth(credentials)
	client, err := mongo.Connect(Ctx, clientOptions)

	if err != nil {
		log.Println(err)
	}
	// Check the connection
	err = client.Ping(Ctx, nil)
	if err != nil {
		log.Println(err)
	}

	fmt.Println("Connected to MongoDB!")
	cidenetDB := client.Database("cidenetDB")
	EmployeesCollection = cidenetDB.Collection("employees")

	return client
}

// function to insert an employee in the database
func CreateEmployee(newEmployee models.Employee) error {

	_, err := EmployeesCollection.InsertOne(Ctx, newEmployee)

	if err != nil {
		return err
	}

	log.Println("Created succesfully")

	return nil
}

// function to returns an employee from database
func GetEmployeeByID(id string) models.Employee {
	var employeeDB models.Employee
	idPrimitive, err := primitive.ObjectIDFromHex(id)

	if err != nil {

		log.Println(err)
		return employeeDB
	}

	err = EmployeesCollection.FindOne(Ctx, bson.M{"_id": idPrimitive}).Decode(&employeeDB)
	if err != nil {
		log.Println(err)
		return employeeDB
	}

	return employeeDB
}

// function that returns all the employees from database
func GetAllEmployees() ([]primitive.M, error) {

	var employeesArray []primitive.M

	cursor, err := EmployeesCollection.Find(Ctx, bson.M{})
	if err != nil {
		return employeesArray, err
	}
	for cursor.Next(context.TODO()) {
		var employeeDB primitive.M

		cursor.Decode(&employeeDB)
		if err != nil {
			log.Println(err)
			return employeesArray, err
		}

		employeesArray = append(employeesArray, employeeDB)
	}

	return employeesArray, nil
}

// function to count the number of existing emails with the given names
func GetEmailByNames(firstName string, firstLastName string) (int, error) {
	var emailArray []primitive.M

	cursor, err := EmployeesCollection.Find(Ctx, bson.M{"first_name": firstName, "first_last_name": firstLastName})
	if err != nil {
		log.Println(err)
		return -1, err
	}

	if err = cursor.All(Ctx, &emailArray); err != nil {
		log.Println(err)
		return -1, err
	}

	numberOfEmails := len(emailArray)
	if numberOfEmails == 0 {
		return 0, nil
	}

	return numberOfEmails, nil
}

// function to update an employee from database
func UpdateEmployee(newEmployee models.Employee) models.Employee {
	var employeeDB models.Employee

	err := EmployeesCollection.FindOne(Ctx, bson.M{"_id": newEmployee.ID}).Decode(&employeeDB)
	// err := EmployeesCollection.FindOne(Ctx, bson.M{"email": newEmployee.Email}).Decode(&employeeDB)
	if err != nil {
		log.Println("Employee not found.")
		log.Println(err)
		return employeeDB
	}

	newEmployee.ID = employeeDB.ID
	newEmployee.RegisterDate = employeeDB.RegisterDate

	log.Println("update que llega pa actualizar:\n", newEmployee)
	_, err = EmployeesCollection.UpdateOne(
		Ctx,
		bson.M{"_id": newEmployee.ID},
		bson.M{"$set": newEmployee},
	)
	if err != nil {

		log.Println(err)
		return employeeDB
	}
	log.Println("Updated succesfully")

	return newEmployee

}

// function to delete an employee by id from the database
func DeleteEmployee(id string) error {

	primitiveID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
		return err
	}

	_, err = EmployeesCollection.DeleteOne(Ctx, bson.M{"_id": primitiveID})
	if err != nil {
		log.Println(err)
		return err
	}

	log.Println("Deleted succesfully")

	return nil
}

// function to verify if the Identification number and Identification Type already exists.
func ExistIDnumAndIDType(idEmployee primitive.ObjectID, idNum, idType string) bool {
	var employeeDB models.Employee

	err := EmployeesCollection.FindOne(Ctx, bson.M{"id_number": idNum, "id_type": idType}).Decode(&employeeDB)
	if err != nil {
		log.Println(err)
	}

	if employeeDB.ID == primitive.NilObjectID || employeeDB.ID == idEmployee {
		return false
	}

	return true
}
