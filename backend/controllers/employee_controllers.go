package controllers

import (
	"employees/database"
	"employees/models"
	"employees/validations"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gorilla/mux"
)

// router that contains all the endpoints
func Router() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/employees", GetAllEmployees).Methods("GET")
	router.HandleFunc("/employee", CreateEmployee).Methods("POST")
	router.HandleFunc("/employee/{id}", GetEmployeeByID).Methods("GET")
	router.HandleFunc("/employee/{id}", UpdateEmployee).Methods("PUT")
	router.HandleFunc("/employee/{id}", DeleteEmployee).Methods("DELETE")

	return router
}

// returns all the employees
func GetAllEmployees(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	employees, err := database.GetAllEmployees()

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee not found")
		return
	}
	json.Marshal(employees)
	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(employees)
}

// returns an employee by the given ID
func GetEmployeeByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := mux.Vars(r)["id"]

	employee := database.GetEmployeeByID(id)
	// log.Println("emp es: ", employee)

	if employee.ID == primitive.NilObjectID {
		// log.Println("No haaaaaaaaaaaaay")
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee not found")
		return
	}

	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(employee)

}

// creates and insert a new employee
func CreateEmployee(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	requestBody, _ := ioutil.ReadAll(r.Body)
	var employee models.Employee

	json.Unmarshal(requestBody, &employee)

	employee.ID = primitive.NewObjectID()

	// verify ID Number and ID Type to avoid duplicates on employees
	exist := database.ExistIDnumAndIDType(employee.ID, employee.IDNumber, employee.IDType)

	if exist {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("ID Number and ID Type already exists with another employee. Send different data")
		return
	}

	employee.RegisterDate = time.Now().Format("02/01/2006 15:04:05")
	newEmployee, resultValidations := validations.ValidateUser(employee)

	if resultValidations != "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resultValidations)
		return
	}

	employeeComplete, err := validations.CreateEmployeeEmail(newEmployee)
	if err != nil || employeeComplete.Email == "" {

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(err)
		return
	}

	err = database.CreateEmployee(employeeComplete)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(employeeComplete)
}

// deletes an employee by the given ID
func DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := mux.Vars(r)["id"]

	employee := database.GetEmployeeByID(id)

	if employee.ID == primitive.NilObjectID {
		// log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee not found")
		return
	}

	err := database.DeleteEmployee(id)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(err)

	} else {
		w.WriteHeader(http.StatusAccepted)
		json.NewEncoder(w).Encode("Deleted succesfully")
	}
}

// updates an employee
func UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var newEmployeeUpdated models.Employee
	id := mux.Vars(r)["id"]
	oldEmployeeDB := database.GetEmployeeByID(id)

	if oldEmployeeDB.ID == primitive.NilObjectID {
		// log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("Employee not found")
		return
	}

	requestBody, _ := ioutil.ReadAll(r.Body)

	json.Unmarshal(requestBody, &newEmployeeUpdated)
	newEmployeeUpdated.ID, _ = primitive.ObjectIDFromHex(id)

	// verify ID Number and ID Type to avoid duplicates on employees
	exist := database.ExistIDnumAndIDType(newEmployeeUpdated.ID, newEmployeeUpdated.IDNumber, newEmployeeUpdated.IDType)

	if exist {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("ID Number and ID Type already exists with another employee. Send different data")
		return
	}

	// here avoid the user to modify the register date
	newEmployeeUpdated.RegisterDate = oldEmployeeDB.RegisterDate
	newEmployee, resultValidations := validations.ValidateUser(newEmployeeUpdated)

	if resultValidations != "" {
		log.Println(resultValidations)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resultValidations)
		return
	}

	// if oldEmployeeDB.FirstName != newEmployee.FirstName || oldEmployeeDB.FirstLastName != newEmployee.FirstLastName {

	newEmployeeWithEmail, err := validations.CreateEmployeeEmail(newEmployee)

	if err != nil {
		log.Println(err)
	}
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	json.NewEncoder(w).Encode(err)
	// 	return
	// }
	// log.Println("User actualizado es: ", newEmployeeWithEmail)

	newEmployee = newEmployeeWithEmail
	// }

	newEmployee.UpdateDate = time.Now().Format("02/01/2006 15:04:05")
	employeeUpdated := database.UpdateEmployee(newEmployee)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(employeeUpdated)
}
