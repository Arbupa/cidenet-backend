package validations

import (
	"employees/database"
	"employees/models"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"
)

// func to check if "check" date is between 2 dates
func isTimeBetween(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

// function that creates the email for user
func CreateEmployeeEmail(employee models.Employee) (models.Employee, error) {

	var email string
	numOfEmails, err := database.GetEmailByNames(employee.FirstName, employee.FirstLastName)

	if err != nil {
		log.Println(err)
		return employee, err
	}

	if numOfEmails == 0 {
		email = fmt.Sprintf("%s.%s@%s", strings.ToLower(employee.FirstName), strings.ToLower(employee.FirstLastName), employee.Domain)
	}
	if numOfEmails > 0 {
		email = fmt.Sprintf("%s.%s.%d@%s", strings.ToLower(employee.FirstName), strings.ToLower(employee.FirstLastName), numOfEmails, employee.Domain)
	}

	if len(email) > 300 {
		log.Println("names of email must be lower than 300 characters")
		email = ""
		errEmail := errors.New("error: names of email must be lower than 300 characters")
		return employee, errEmail
	}
	employee.Email = email

	return employee, nil
}

// function to validate most of the data from the user
func ValidateUser(employee models.Employee) (models.Employee, string) {

	var errorMessage string
	var validName = regexp.MustCompile(`^[A-Z ]+$`)
	var IDTypesAllowed = []string{"Cedula de Ciudadania", "Cedula de Extranjeria", "Pasaporte", "Permiso Especial"}
	var areasAllowed = []string{"Administracion", "Financiera", "Compras", "Infraestructura", "Operacion", "Talento Humano", "Servicios Varios"}
	var validIDNum = regexp.MustCompile(`^[A-z0-9-]+$`)
	var CountriesAndDomains = map[string]string{
		"Colombia":       "cidenet.com.co",
		"Estados Unidos": "cidenet.com.us",
	}

	// check if the given names make match with the regular expressions
	if !validName.MatchString(employee.FirstLastName) || len(employee.FirstLastName) > 20 {
		errorMessage += "First Last Name field not valid. "
	}
	if !validName.MatchString(employee.SecondLastName) || len(employee.SecondLastName) > 20 {
		errorMessage += "Second Last Name field not valid. "
	}
	if !validName.MatchString(employee.FirstName) || len(employee.FirstName) > 20 {
		errorMessage += "First Name field not valid. "
	}

	if employee.OtherNames != "" {

		if !validName.MatchString(employee.OtherNames) || len(employee.OtherNames) > 50 {
			errorMessage += "Other Name field not valid. "
		}
	}

	// remove all possible whitespaces
	employee.FirstLastName = strings.Join(strings.Fields(employee.FirstLastName), "")
	employee.SecondLastName = strings.Join(strings.Fields(employee.SecondLastName), "")
	employee.FirstName = strings.Join(strings.Fields(employee.FirstName), "")
	employee.OtherNames = strings.Join(strings.Fields(employee.OtherNames), "")

	domainValue, countryFound := CountriesAndDomains[employee.CountryWorking]

	if !countryFound {
		errorMessage += "Country field not valid. "
	}

	if domainValue == "" {

		domain := false

		for _, v := range CountriesAndDomains {
			if v == employee.Domain {
				domain = true
				break
			}
		}

		if !domain {
			errorMessage += "Domain field not valid. "
		}
	}

	employee.Domain = domainValue
	IDTypeExists := false

	for _, v := range IDTypesAllowed {
		if v == employee.IDType {
			IDTypeExists = true
			break
		}
	}

	if !IDTypeExists {
		errorMessage += "Identification type field not valid. "
	}

	if !validIDNum.MatchString(employee.IDNumber) || len(employee.OtherNames) > 20 {
		errorMessage += "Identification number field not valid. "
	}

	// here I parse the dates to time.Time and make some operations to finally convert again to string
	// and store that value
	if employee.RegisterDate == "" {
		employee.RegisterDate = time.Now().Format("02/01/2006 15:04:05")
	}

	registerDateEmployee, err := time.Parse("02/01/2006 15:04:05", employee.RegisterDate)
	if err != nil {
		errorMessage += "Invalid Register Date. "
	}
	admissionDateEmployee, err := time.Parse("02/01/2006 15:04:05", employee.AdmissionDate)
	if err != nil {
		errorMessage += "Invalid Admission Date. "
	}

	start := registerDateEmployee.AddDate(0, -1, 0)

	if !isTimeBetween(start, registerDateEmployee, admissionDateEmployee) {
		errorMessage += "Admission date field must be at most a month less since the register date. "
	}

	employee.State = "Activo"

	areaExists := false
	for _, v := range areasAllowed {
		if v == employee.Area {
			areaExists = true
			break
		}
	}

	if !areaExists {
		errorMessage += "Area field not valid. "
	}

	return employee, errorMessage
}
